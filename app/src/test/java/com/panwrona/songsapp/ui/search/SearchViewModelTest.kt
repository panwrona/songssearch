package com.panwrona.songsapp.ui.search

import com.nhaarman.mockito_kotlin.times
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.panwrona.domain.search.SongsSource
import com.panwrona.domain.search.usecase.SearchForSongUseCase
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchViewModelTest {

    @Mock
    lateinit var searchForSongUseCase: SearchForSongUseCase
    private lateinit var searchViewModel: SearchViewModel

    @Before
    fun setUp() {
        searchViewModel = SearchViewModel(searchForSongUseCase)
    }

    @Test
    fun `should not execute search for songs useCase when search query is same as the last one`() {
        //Given
        searchViewModel.searchQuery = "abc"

        //When
        val testObserver = searchViewModel
                .observeSearch(
                        Observable.just("abc"),
                        SongsSource.ItunesAndLocal).test()

        //Then
        verifyZeroInteractions(searchForSongUseCase)
        testObserver.assertNoValues()
    }

    @Test
    fun `should execute search for songs useCase when search query is different as the last one`() {
        //Given
        searchViewModel.searchQuery = "abc"
        val newSearchQuery = "abcd"
        whenever(searchForSongUseCase.execute(SongsSource.ItunesAndLocal, newSearchQuery)).thenReturn(Observable.just(emptyList()))

        //When
        val testObserver = searchViewModel
                .observeSearch(
                        Observable.just(newSearchQuery),
                        SongsSource.ItunesAndLocal).test()

        //Then
        verify(searchForSongUseCase).execute(SongsSource.ItunesAndLocal, newSearchQuery)
        assertTrue(searchViewModel.searchQuery == newSearchQuery)
        testObserver.assertValue { it.isEmpty() }
    }

    @Test
    fun `should show progress bar and return empty list when different query is typed and useCase returns empty list`() {
        //Given
        searchViewModel.searchQuery = "abc"
        val uiStateObserver = TestObserver<SearchUIState>()
        searchViewModel.uiStateSubject.subscribe(uiStateObserver)
        val newSearchQuery = "abcd"
        whenever(searchForSongUseCase.execute(SongsSource.ItunesAndLocal, newSearchQuery)).thenReturn(Observable.just(emptyList()))

        //When
        val testObserver = searchViewModel
                .observeSearch(
                        Observable.just(newSearchQuery),
                        SongsSource.ItunesAndLocal).test()

        //Then
        uiStateObserver.assertValues(SearchUIState.Loading, SearchUIState.EmptySongsList)
        testObserver.assertNotComplete()
    }

    @Test
    fun `should emit error state and should not terminate when error occurs`() {
        //Given
        searchViewModel.searchQuery = "abc"
        val uiStateObserver = TestObserver<SearchUIState>()
        searchViewModel.uiStateSubject.subscribe(uiStateObserver)
        val newSearchQuery = "abcd"
        val throwableMessage = "error"
        whenever(searchForSongUseCase.execute(SongsSource.ItunesAndLocal, newSearchQuery)).thenReturn(Observable.error(Throwable(throwableMessage)))

        //When
        val testObserver = searchViewModel
                .observeSearch(
                        Observable.just(newSearchQuery),
                        SongsSource.ItunesAndLocal).test()

        //Then
        uiStateObserver.assertValues(SearchUIState.Loading, SearchUIState.Error(throwableMessage))
        testObserver.assertNotComplete()
        testObserver.assertNotTerminated()
        testObserver.assertNoErrors()
    }

    @Test
    fun `should retry call with same query when retry is called`() {
        //Given
        searchViewModel.searchQuery = "abc"
        val newSearchQuery = "abcd"
        whenever(searchForSongUseCase.execute(SongsSource.ItunesAndLocal, newSearchQuery)).thenReturn(Observable.just(emptyList()))

        //When
        searchViewModel
                .observeSearch(
                        Observable.just(newSearchQuery),
                        SongsSource.ItunesAndLocal).test()
        searchViewModel.retry()

        //Then
        verify(searchForSongUseCase, times(2)).execute(SongsSource.ItunesAndLocal, newSearchQuery)
    }
}