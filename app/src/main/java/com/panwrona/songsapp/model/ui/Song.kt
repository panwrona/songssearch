package com.panwrona.songsapp.model.ui

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
open class Song : Parcelable

@Parcelize
data class ItunesSong(val title: String, val artist: String, val description: String) : Song(), Parcelable

@Parcelize
data class LocalSong(val title: String, val artist: String, val description: String) : Song(), Parcelable