package com.panwrona.songsapp.widget.recyclerview

import android.content.Context
import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.View

class PaddingItemDecoration(context: Context, padding: Float) : RecyclerView.ItemDecoration() {

    private val padding: Int

    init {
        val metrics = context.resources.displayMetrics
        this.padding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, padding, metrics).toInt()
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State?) {
        val itemPosition = parent.getChildAdapterPosition(view)
        if (itemPosition == RecyclerView.NO_POSITION) {
            return
        }
        if (itemPosition == 0) {
            outRect.top = this.padding
        }
        outRect.bottom = padding
    }
}