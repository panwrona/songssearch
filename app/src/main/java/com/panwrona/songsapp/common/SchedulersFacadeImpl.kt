package com.panwrona.songsapp.common

import com.panwrona.domain.common.SchedulersFacade
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
 /*
  * Facade pattern used to provide schedulers to usecases. I often use this approach, since the domain layer has
  * no information about f.e. android dependencies
  */
object  SchedulersFacadeImpl : SchedulersFacade {
    override fun main() = AndroidSchedulers.mainThread()

    override fun io() = Schedulers.io()
}