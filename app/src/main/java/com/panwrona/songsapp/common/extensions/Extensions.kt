import android.content.res.Resources
import android.view.View

/**
  *
  * Normally I would create file for each component, f.e. ViewExtensions, ContextExtensions etc.
  *
  **/

fun View.gone() {
    visibility = View.GONE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.visible() {
    visibility = View.VISIBLE
}

val Int.dp: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()
val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()