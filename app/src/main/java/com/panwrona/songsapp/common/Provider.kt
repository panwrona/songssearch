package com.panwrona.songsapp.common

import com.panwrona.domain.common.SchedulersFacade
import com.panwrona.domain.repository.ItunesRepository
import com.panwrona.domain.repository.LocalRepository
import com.panwrona.domain.search.usecase.SearchForSongUseCase
import com.panwrona.local.repository.LocalRepositoryImpl
import com.panwrona.rest.ItunesApiManager
import com.panwrona.songsapp.ui.search.SearchViewModel

/*
  * On the daily basis I use Dagger 2 for Dependency Injection. I thought I would try Kodein in this project, but:
  * 1. I don't have too much time right now :P
  * 2. I think Dagger 2 is still better framework, it is way more loosely coupled to the codebase than Kodein.
  * Also Kodein looks like just manual 'service-locator-like' framework, where we have to either inject dependencies by ourselves
  * or have to be tightly coupled to the framework
 */
object Provider {

    fun provideSearchViewModel(): SearchViewModel {
        return SearchViewModel(provideSearchFoSongUseCase())
    }

    fun provideSearchFoSongUseCase(): SearchForSongUseCase {
        return SearchForSongUseCase(
                provideItunesRepository(),
                provideLocalRepository(),
                provideSchedulersFacade()
        )
    }

    private fun provideSchedulersFacade(): SchedulersFacade {
        return SchedulersFacadeImpl
    }

    private fun provideLocalRepository(): LocalRepository {
        return LocalRepositoryImpl
    }

    private fun provideItunesRepository(): ItunesRepository {
        return ItunesApiManager
    }
}