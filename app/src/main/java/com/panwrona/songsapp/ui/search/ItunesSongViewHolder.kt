package com.panwrona.songsapp.ui.search

import android.support.v7.widget.RecyclerView
import android.view.View
import com.panwrona.songsapp.R
import com.panwrona.songsapp.model.ui.ItunesSong
import kotlinx.android.synthetic.main.recycler_item_itunes_song.view.*

/*
 * I don't use single expression function when it doesn't return anything
 */

class ItunesSongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(itunesSong: ItunesSong, onSongClicked: OnSongClickListener) {
        with(itemView) {
            itemView.setOnClickListener { onSongClicked(itunesSong, itemView) }
            titleTextView.text = String.format(itemView.resources.getString(R.string.title), itunesSong.title)
            artistTextView.text = String.format(itemView.resources.getString(R.string.artist), itunesSong.title)
        }
    }
}