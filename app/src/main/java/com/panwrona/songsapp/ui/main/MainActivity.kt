package com.panwrona.songsapp.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.panwrona.songsapp.R
import com.panwrona.songsapp.ui.search.SearchActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchIcon.setOnClickListener {
            startActivity(SearchActivity.getIntent(this, searchIcon))
            overridePendingTransition(0, 0)
        }
    }
}