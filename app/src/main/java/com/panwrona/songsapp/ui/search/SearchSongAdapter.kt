package com.panwrona.songsapp.ui.search

import android.support.v4.view.ViewCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.panwrona.songsapp.R
import com.panwrona.songsapp.model.ui.ItunesSong
import com.panwrona.songsapp.model.ui.LocalSong
import com.panwrona.songsapp.model.ui.Song

typealias OnSongClickListener = (Song, View) -> Unit
private const val TYPE_ITUNES = 0
private const val TYPE_LOCAL = 1
private const val TYPE_UNDEFINED = 2
class SearchSongAdapter(private var songsList: List<Song>, private val onSongClicked: OnSongClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = when(viewType) {
            TYPE_ITUNES  -> ItunesSongViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_itunes_song, parent, false))
            TYPE_LOCAL -> LocalSongViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_local_song, parent, false))
            TYPE_UNDEFINED -> UndefinedViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_undefined, parent, false))
            else -> UndefinedViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_undefined, parent, false))
    }

    override fun getItemCount() = songsList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = songsList[holder.adapterPosition]
        ViewCompat.setTransitionName(holder.itemView, holder.adapterPosition.toString())
        when(item) {
            is ItunesSong -> (holder as ItunesSongViewHolder).bind(item, onSongClicked)
            is LocalSong -> (holder as LocalSongViewHolder).bind(item, onSongClicked)
        }
    }

    override fun getItemViewType(position: Int): Int = when(songsList[position]) {
        is ItunesSong -> TYPE_ITUNES
        is LocalSong -> TYPE_LOCAL
        else -> TYPE_UNDEFINED
    }

    fun setList(songsList: List<Song>) {
        this.songsList = songsList
        notifyDataSetChanged()
    }
}