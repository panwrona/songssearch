package com.panwrona.songsapp.ui.search

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import android.view.ViewAnimationUtils
import android.view.ViewTreeObserver
import android.view.animation.AccelerateInterpolator
import android.view.inputmethod.InputMethodManager
import com.jakewharton.rxbinding2.widget.RxTextView
import com.panwrona.domain.search.SongsSource
import com.panwrona.songsapp.R
import com.panwrona.songsapp.common.Provider
import com.panwrona.songsapp.model.ui.Song
import com.panwrona.songsapp.ui.details.DetailsActivity
import com.panwrona.songsapp.widget.recyclerview.PaddingItemDecoration
import dp
import gone
import invisible
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_search.*
import visible
import java.util.concurrent.TimeUnit

class SearchActivity : AppCompatActivity() {

    private lateinit var searchViewModel: SearchViewModel
    private var searchSongAdapter: SearchSongAdapter? = null
    private val compositeDisposable = CompositeDisposable()

    companion object {
        private const val EXTRA_CIRCULAR_REVEAL_X = "EXTRA_CIRCULAR_REVEAL_X"
        private const val EXTRA_CIRCULAR_REVEAL_Y = "EXTRA_CIRCULAR_REVEAL_Y"

        private fun getSearchIconCoords(view: View): Pair<Int, Int> {
            val centerX = (view.x + view.width / 2).toInt()
            val centerY = (view.y + view.height / 2).toInt() + 22.dp
            return centerX to centerY
        }

        fun getIntent(context: Context, view: View) = Intent(context, SearchActivity::class.java).apply {
            val iconCoords = getSearchIconCoords(view)
            putExtra(EXTRA_CIRCULAR_REVEAL_X, iconCoords.first)
            putExtra(EXTRA_CIRCULAR_REVEAL_Y, iconCoords.second)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        searchViewModel = Provider.provideSearchViewModel()
        window.enterTransition = null
        val haveNoSavedInstances = savedInstanceState == null
        setupEnterAnimation(isAnimationNeeded = haveNoSavedInstances && hasCircularRevealExtras())
        setupSearch()
        setupRecyclerView()
        setupToolbar()
        setupUiStateDispatcher()
        setupErrorScreen()
    }

    private fun setupToolbar() {
        toolbar.setNavigationOnClickListener { finishSearchScreen() }
    }

    private fun setupErrorScreen() {
        retryButton.setOnClickListener {
            searchViewModel.retry()
        }
    }

    private fun setupRecyclerView() {
        searchRecyclerView.layoutManager = LinearLayoutManager(this)
        searchRecyclerView.addItemDecoration(PaddingItemDecoration(this, padding = 8f))
        searchRecyclerView.itemAnimator = DefaultItemAnimator()
    }

    private fun setupUiStateDispatcher() {
        searchViewModel.uiStateSubject.subscribe {
            dispatch(it)
        }
    }

    private fun dispatch(it: SearchUIState?) {
        setKeyboardVisibility(isVisible = false)
        when(it) {
            is SearchUIState.Error -> showErrorScreen(it.message)
            is SearchUIState.Loading -> showLoadingScreen()
            is SearchUIState.SongsLoaded -> showLoadedSongsScreen(it.songs)
            is SearchUIState.EmptySongsList -> showEmptySongsListScreen()
        }
    }

    private fun showEmptySongsListScreen() {
        searchRecyclerView.gone()
        errorContainer.gone()
        loadingProgressBar.invisible()
        emptySongsListContainer.visible()
    }

    private fun showLoadedSongsScreen(songs: List<Song>) {
        searchRecyclerView.visible()
        emptySongsListContainer.gone()
        loadingProgressBar.invisible()
        errorContainer.gone()
        setupSongsAdapter(songs)
    }

    private fun setupSongsAdapter(songs: List<Song>) {
        if (searchSongAdapter == null) {
            searchSongAdapter = SearchSongAdapter(songs) { song, view ->
                showSongDetails(song, view)
            }
            searchRecyclerView.adapter = searchSongAdapter
        } else {
            searchSongAdapter?.setList(songs)
        }
    }

    private fun showLoadingScreen() {
        searchRecyclerView.gone()
        emptySongsListContainer.gone()
        errorContainer.gone()
        loadingProgressBar.visible()
    }

    private fun showErrorScreen(message: String) {
        searchRecyclerView.gone()
        loadingProgressBar.invisible()
        emptySongsListContainer.gone()
        errorContainer.visible()
        errorTextView.text = message
    }

    private fun setupSearch() {
        val searchObserver = RxTextView.afterTextChangeEvents(searchBox)
                .skipInitialValue()
                .map { it.view().text.toString() }
                .debounce(500, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())

        compositeDisposable
                .add(searchViewModel
                .observeSearch(searchObserver, SongsSource.ItunesAndLocal)
                .subscribe()
        )
    }

    private fun showSongDetails(song: Song, view: View) {
        val intent = DetailsActivity.getIntent(this, view.z, view.transitionName, song)
        val option = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view, view.transitionName).toBundle()
        startActivity(intent, option)
    }

    override fun onBackPressed() {
        showParentWithLeavingAnimation()
    }

    override fun onOptionsItemSelected(item: MenuItem) =
            when (item.itemId) {
                android.R.id.home -> finishSearchScreen()
                else -> super.onOptionsItemSelected(item)
            }

    private fun finishSearchScreen(): Boolean {
        setKeyboardVisibility(isVisible = false)
        showParentWithLeavingAnimation()
        return true
    }

    private fun setupEnterAnimation(isAnimationNeeded: Boolean) {
        if (isAnimationNeeded) {
           buildWithViewTreeObserver(container) {
                showActivityAnimation(isEntering = true) {
                    searchBox.requestFocus()
                    setKeyboardVisibility(isVisible = true)
                }
            }
        }
    }

    private fun buildWithViewTreeObserver(animatedView: View, action: () -> Unit) {
        val viewTreeObserver = animatedView.viewTreeObserver
        if (viewTreeObserver.isAlive) {
            addOnGlobalLayoutListener(viewTreeObserver, animatedView, action)
        }
    }

    private fun addOnGlobalLayoutListener(viewTreeObserver: ViewTreeObserver, animatedView: View, action: () -> Unit) {
        viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                animatedView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                action()
            }
        })
    }

    private fun showParentWithLeavingAnimation() {
        showActivityAnimation(isEntering = false) {
            toolbar.visibility = View.INVISIBLE
            container.gone()
            finish()
            overridePendingTransition(0, 0)
        }
    }

    private fun showActivityAnimation(isEntering: Boolean, endAction: (() -> Unit)) {
        val (centerX, centerY) = getAnimationCenterCoordinates()
        val toolbarAnimator = buildCircularReveal(container, centerX, centerY, isEntering)
        val contentAnimator = getContentAnimator(searchContainer, isEntering)

        playAnimatorsTogether(toolbarAnimator, contentAnimator, endAction)
    }

    private fun getContentAnimator(contentView: View, isGrowing: Boolean): ObjectAnimator {
        val fromAlpha = if (isGrowing) 0f else 1f
        val toAlpha = if (isGrowing) 1f else 0f
        return ObjectAnimator.ofFloat(contentView, "alpha", fromAlpha, toAlpha).apply {
            interpolator = AccelerateInterpolator()
            duration = 300
        }
    }

    private fun playAnimatorsTogether(toolbarAnimator: Animator, contentAnimator: ObjectAnimator, endAction: () -> Unit) {
        with(AnimatorSet()) {
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animator: Animator) {
                    endAction()
                }
            })
            playTogether(toolbarAnimator, contentAnimator)
            start()
        }
    }

    private fun setKeyboardVisibility(isVisible: Boolean) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (isVisible) {
            imm.showSoftInput(searchBox, InputMethodManager.SHOW_IMPLICIT)
        } else {
            imm.hideSoftInputFromWindow(searchBox.windowToken, 0)
        }
    }

    private fun getAnimationCenterCoordinates(): Pair<Int, Int> {
        val centerX = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_X, 0)
        val centerY = intent.getIntExtra(EXTRA_CIRCULAR_REVEAL_Y, 0)
        return Pair(centerX, centerY)
    }

    private fun buildCircularReveal(animatedView: View, centerX: Int, centerY: Int, isGrowing: Boolean): Animator {
        val finalRadius = (Math.max(animatedView.width, animatedView.height)).toFloat()
        val revealAnimation = createRevealAnimation(isGrowing, animatedView, centerX, centerY, finalRadius)
        revealAnimation.interpolator = AccelerateInterpolator()

        return revealAnimation
    }

    private fun createRevealAnimation(isGrowing: Boolean, animatedView: View, centerX: Int, centerY: Int, finalRadius: Float): Animator {
        return if (isGrowing) {
            ViewAnimationUtils.createCircularReveal(animatedView, centerX, centerY, 0f, finalRadius)
        } else {
            ViewAnimationUtils.createCircularReveal(animatedView, centerX, centerY, finalRadius, 0f)
        }
    }

    private fun hasCircularRevealExtras() =
            intent.hasExtra(EXTRA_CIRCULAR_REVEAL_X) && intent.hasExtra(EXTRA_CIRCULAR_REVEAL_Y)

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}