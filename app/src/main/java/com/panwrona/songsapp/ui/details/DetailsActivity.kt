package com.panwrona.songsapp.ui.details

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.*
import android.view.Gravity
import android.view.ViewTreeObserver
import android.view.WindowManager
import com.panwrona.songsapp.R
import com.panwrona.songsapp.common.transition.TransitionAdapter
import com.panwrona.songsapp.model.ui.ItunesSong
import com.panwrona.songsapp.model.ui.LocalSong
import com.panwrona.songsapp.model.ui.Song
import kotlinx.android.synthetic.main.activity_details.*
import visible

class DetailsActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_Z_INDEX = "EXTRA_Z_INDEX"
        private const val EXTRA_TRANSITION_NAME = "EXTRA_TRANSITION_NAME"
        private const val EXTRA_SONG = "EXTRA_SONG"

        fun getIntent(context: Context, zIndex: Float, transitionName: String, song: Song): Intent {
            return Intent(context, DetailsActivity::class.java).apply {
                putExtra(EXTRA_Z_INDEX, zIndex)
                putExtra(EXTRA_TRANSITION_NAME, transitionName)
                putExtra(EXTRA_SONG, song)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        postponeEnterTransition()
        setupEnterTransition()
        setupReturnTransition()
        setupSongDetails(intent.getParcelableExtra<Song>(EXTRA_SONG))
    }

    private fun setupReturnTransition() {
        val set = TransitionSet()
        val shared = TransitionInflater.from(this).inflateTransition(android.R.transition.move)
        shared.addTarget(appBarLayout)
        shared.startDelay = 100
        set.addTransition(shared)

        val slide = Slide(Gravity.BOTTOM)
        slide.addTarget(nestedScrollView)
        set.addTransition(slide)
        window.returnTransition = set
    }

    private fun setupSongDetails(song: Song?) {
        when(song) {
            is ItunesSong -> setupItunesSong(song)
            is LocalSong -> setupLocalSong(song)
            else -> setupUndefinedSong()
        }
    }

    private fun setupUndefinedSong() {
        val undefined = getString(R.string.undefined)
        songTitleTextView.text = undefined
        songArtistTextView.text = undefined
        toolbar.title = undefined
    }

    private fun setupLocalSong(song: LocalSong) {
        songTitleTextView.text = String.format(getString(R.string.title), song.title)
        songArtistTextView.text = String.format(getString(R.string.artist), song.title)
        toolbar.title = getString(R.string.local_song)
    }

    private fun setupItunesSong(song: ItunesSong) {
        songTitleTextView.text = String.format(getString(R.string.title), song.title)
        songArtistTextView.text = String.format(getString(R.string.artist), song.title)
        toolbar.title = getString(R.string.itunes_song)
    }

    private fun setupEnterTransition() {
        appBarLayout.viewTreeObserver.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                appBarLayout.viewTreeObserver.removeOnPreDrawListener(this)
                appBarLayout.transitionName = intent.getStringExtra(EXTRA_TRANSITION_NAME)
                window.sharedElementEnterTransition.addListener(object : TransitionAdapter() {
                    override fun onTransitionEnd(transition: Transition?) {
                        super.onTransitionEnd(transition)
                        val slide = Slide(Gravity.BOTTOM)
                        TransitionManager.beginDelayedTransition(nestedScrollView, slide)
                        nestedScrollView.visible()
                    }
                })
                startPostponedEnterTransition()
                return true
            }
        })
    }

    override fun onBackPressed() {
        finishAfterTransition()
    }

    override fun finishAfterTransition() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        super.finishAfterTransition()
    }
}