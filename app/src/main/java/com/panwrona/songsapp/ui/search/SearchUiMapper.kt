package com.panwrona.songsapp.ui.search

import com.panwrona.domain.model.DomainItunesSong
import com.panwrona.domain.model.DomainLocalSong
import com.panwrona.domain.model.DomainSong
import com.panwrona.songsapp.model.ui.ItunesSong
import com.panwrona.songsapp.model.ui.LocalSong
import com.panwrona.songsapp.model.ui.Song

object SearchUiMapper {

    fun toUiModel(domainSongs: List<DomainSong>): List<Song> {
        val songs = ArrayList<Song>()
        domainSongs.forEach {
            when(it) {
                is DomainLocalSong -> songs.add(mapToLocalSong(it))
                is DomainItunesSong -> songs.add(mapToItunesSong(it))
            }
        }
        return songs
    }

    private fun mapToItunesSong(it: DomainItunesSong) = ItunesSong(
                title = it.title,
                artist = it.artist,
                description = it.description
    )

    private fun mapToLocalSong(it: DomainLocalSong) = LocalSong(
                title = it.title,
                artist = it.artist,
                description = it.description
    )
}