package com.panwrona.songsapp.ui.search

import com.panwrona.songsapp.model.ui.Song

sealed class SearchUIState {
    object Loading : SearchUIState()
    data class Error(val message: String) : SearchUIState()
    data class SongsLoaded(val songs: List<Song>): SearchUIState()
    object EmptySongsList : SearchUIState()
}
