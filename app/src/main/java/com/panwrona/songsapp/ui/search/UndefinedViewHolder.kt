package com.panwrona.songsapp.ui.search

import android.support.v7.widget.RecyclerView
import android.view.View

// Class is needed by else branch in "when" block
class UndefinedViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
