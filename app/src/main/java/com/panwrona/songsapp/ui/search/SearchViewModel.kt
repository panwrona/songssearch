package com.panwrona.songsapp.ui.search

import com.panwrona.domain.search.SongsSource
import com.panwrona.domain.search.usecase.SearchForSongUseCase
import com.panwrona.songsapp.model.ui.Song
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject

class SearchViewModel(
        private val searchForSongsUseCase: SearchForSongUseCase
) {

    val uiStateSubject = PublishSubject.create<SearchUIState>()
    private val retrySubject = PublishSubject.create<Boolean>()
    var searchQuery = ""

    fun observeSearch(searchObservable: Observable<String>, songsSource: SongsSource): Observable<List<Song>> {
        return Observable.combineLatest(
                        searchObservable.filter { searchQuery != it },
                        retrySubject.startWith(true), //Emit first item to enable combineLatest functionality
                        BiFunction<String, Boolean, String> { query, _ ->
                            query
                        })
                        .map {
                            searchQuery = it
                            uiStateSubject.onNext(SearchUIState.Loading)
                            it
                        }
                        .switchMap {
                            searchForSongsUseCase
                                    .execute(songsSource, it)
                                    .map { SearchUiMapper.toUiModel(it) }
                                    .onErrorResumeNext { throwable: Throwable ->
                                        uiStateSubject.onNext(SearchUIState.Error(throwable.localizedMessage))
                                        Observable.empty()
                                    }
                        }
                        .doOnNext {
                            if (it.isEmpty()) uiStateSubject.onNext(SearchUIState.EmptySongsList)
                            else uiStateSubject.onNext(SearchUIState.SongsLoaded(it))
                        }
    }

    fun retry() {
        retrySubject.onNext(true)
    }
}
