package com.panwrona.songsapp.ui.search

import android.support.v7.widget.RecyclerView
import android.view.View
import com.panwrona.songsapp.R
import com.panwrona.songsapp.model.ui.LocalSong
import kotlinx.android.synthetic.main.recycler_item_local_song.view.*

class LocalSongViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(localSong:  LocalSong, onSongClicked: OnSongClickListener) {
        with(itemView) {
            itemView.setOnClickListener { onSongClicked(localSong, itemView) }
            titleTextView.text = String.format(itemView.resources.getString(R.string.title), localSong.title)
            artistTextView.text = String.format(itemView.resources.getString(R.string.artist), localSong.title)
        }
    }
}