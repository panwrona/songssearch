package com.panwrona.local.repository.model

import com.google.gson.annotations.SerializedName

data class JsonSongsList(
        @SerializedName("songs") val songs: List<JsonSong>
)