package com.panwrona.local.repository

import com.panwrona.domain.model.DomainLocalSong
import com.panwrona.local.repository.model.JsonSong

object LocalMapper {

    fun toDomainLocalSongs(songs: List<JsonSong>): List<DomainLocalSong> {
        val domainSongs = ArrayList<DomainLocalSong>()
        songs.forEach {
            domainSongs.add(DomainLocalSong(
                    artist = it.artist,
                    description = it.description ?: "",
                    title = it.title
            ))
        }
        return domainSongs
    }
}