package com.panwrona.local.repository.model

import com.google.gson.annotations.SerializedName

data class JsonSong(
        @SerializedName("title") val title: String,
        @SerializedName("artist") val artist: String,
        @SerializedName("web_url") val description: String?
)