package com.panwrona.local.repository

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.panwrona.domain.model.DomainLocalSong
import com.panwrona.domain.repository.LocalRepository
import com.panwrona.local.repository.model.JsonSongsList
import com.panwrona.local.repository.model.SongsListProvider
import io.reactivex.Observable

object LocalRepositoryImpl : LocalRepository {

    private val jsonSongs = getSongsFromString()

    private fun getSongsFromString(): JsonSongsList {
        val typeToken = object : TypeToken<JsonSongsList>() {}.getType()
        return Gson().fromJson(SongsListProvider.provideSongsList(), typeToken) as JsonSongsList
    }

    override fun searchForSong(query: String): Observable<List<DomainLocalSong>> {
        val filteredList = jsonSongs.songs.filter { it.artist.contains(query, true) || it.title.contains(query, true) }
        return Observable.just(filteredList)
                .map {
                    LocalMapper.toDomainLocalSongs(it)
                }
    }
}