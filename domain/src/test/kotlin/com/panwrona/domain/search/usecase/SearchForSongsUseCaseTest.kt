package com.panwrona.domain.search.usecase

import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyZeroInteractions
import com.nhaarman.mockito_kotlin.whenever
import com.panwrona.domain.common.SchedulersFacade
import com.panwrona.domain.model.DomainItunesSong
import com.panwrona.domain.model.DomainLocalSong
import com.panwrona.domain.model.DomainSong
import com.panwrona.domain.repository.ItunesRepository
import com.panwrona.domain.repository.LocalRepository
import com.panwrona.domain.search.SongsSource
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class SearchForSongsUseCaseTest {

    @Mock lateinit var localRepository: LocalRepository
    @Mock lateinit var itunesRepository: ItunesRepository
    @Mock lateinit var schedulersFacade: SchedulersFacade

    private lateinit var useCase: SearchForSongUseCase
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        whenever(schedulersFacade.io()).thenReturn(Schedulers.trampoline())
        whenever(schedulersFacade.main()).thenReturn(Schedulers.trampoline())
        useCase = SearchForSongUseCase(itunesRepository, localRepository, schedulersFacade)

    }

    @Test
    fun `should not search in itunes repository when songs source is local`() {
        //Given
        val query = "test"
        whenever(localRepository.searchForSong(any())).thenReturn(Observable.empty())
        //When
        useCase.execute(SongsSource.Local, query).subscribe(TestObserver<List<DomainSong>>())
        //Then
        verify(localRepository).searchForSong(query)
        verifyZeroInteractions(itunesRepository)
    }

    @Test
    fun `should not search in local repository when songs source is itunes`() {
        //Given
        val query = "test"
        whenever(itunesRepository.searchForSong(any())).thenReturn(Observable.empty())

        //When
        useCase.execute(SongsSource.Itunes, query).subscribe(TestObserver<List<DomainSong>>())

        //Then
        verify(itunesRepository).searchForSong(query)
        verifyZeroInteractions(localRepository)
    }

    @Test
    fun `should search in both local and itunes repositories when songs source is itunes and local`() {
        //Given
        val query = "test"
        whenever(itunesRepository.searchForSong(any())).thenReturn(Observable.empty())
        whenever(localRepository.searchForSong(any())).thenReturn(Observable.empty())

        //When
        useCase.execute(SongsSource.ItunesAndLocal, query).subscribe(TestObserver<List<DomainSong>>())

        //Then
        verify(itunesRepository).searchForSong(query)
        verify(localRepository).searchForSong(query)
    }

    @Test
    fun `should two lists when both locall and itunes repositories are queried `() {
        val query = "test"
        whenever(itunesRepository.searchForSong(any())).thenReturn(Observable.just(arrayListOf(DomainItunesSong("","",""))))
        whenever(localRepository.searchForSong(any())).thenReturn(Observable.just(arrayListOf(DomainLocalSong("","",""))))

        val testObserver = useCase.execute(SongsSource.ItunesAndLocal, query).test()

        testObserver.assertValue { it[0] is DomainItunesSong && it[1] is DomainLocalSong }
    }
}