package com.panwrona.domain.model

open class DomainSong
data class DomainItunesSong(val artist: String, val title: String, val description: String) : DomainSong()
data class DomainLocalSong(val artist: String, val title: String, val description: String) : DomainSong()