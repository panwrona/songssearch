package com.panwrona.domain.repository

import com.panwrona.domain.model.DomainLocalSong
import io.reactivex.Observable

interface LocalRepository {
    fun searchForSong(query: String) : Observable<List<DomainLocalSong>>
}