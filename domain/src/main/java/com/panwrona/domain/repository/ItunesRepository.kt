package com.panwrona.domain.repository

import com.panwrona.domain.model.DomainItunesSong
import io.reactivex.Observable

interface ItunesRepository {
    fun searchForSong(query: String) : Observable<List<DomainItunesSong>>
}