package com.panwrona.domain.search

enum class SongsSource {
    Itunes, Local, ItunesAndLocal
}