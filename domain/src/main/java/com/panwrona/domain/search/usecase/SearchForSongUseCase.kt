package com.panwrona.domain.search.usecase

import com.panwrona.domain.common.SchedulersFacade
import com.panwrona.domain.model.DomainItunesSong
import com.panwrona.domain.model.DomainLocalSong
import com.panwrona.domain.model.DomainSong
import com.panwrona.domain.repository.ItunesRepository
import com.panwrona.domain.repository.LocalRepository
import com.panwrona.domain.search.SongsSource
import io.reactivex.Observable
import io.reactivex.functions.BiFunction

class SearchForSongUseCase(
        private val itunesRepository: ItunesRepository,
        private val localRepository: LocalRepository,
        private val schedulersFacade: SchedulersFacade
) {

    fun execute(songsSource: SongsSource, query: String): Observable<out List<DomainSong>> {
        return when (songsSource) {
            SongsSource.Itunes -> itunesRepository.searchForSong(query).addSchedulers(schedulersFacade)
            SongsSource.Local -> localRepository.searchForSong(query).addSchedulers(schedulersFacade)
            SongsSource.ItunesAndLocal -> Observable.zip(
                    itunesRepository.searchForSong(query),
                    localRepository.searchForSong(query),
                    BiFunction<List<DomainItunesSong>, List<DomainLocalSong>, List<DomainSong>> {
                        iTunesSongs: List<DomainItunesSong>, localSongs: List<DomainLocalSong> ->
                        iTunesSongs + localSongs
                    }).addSchedulers(schedulersFacade)
        }
    }

    private fun <T> Observable<T>.addSchedulers(schedulersFacade: SchedulersFacade): Observable<T>
            = this.subscribeOn(schedulersFacade.io()).observeOn(schedulersFacade.main())
}