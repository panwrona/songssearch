package com.panwrona.domain.common

import io.reactivex.Scheduler

interface SchedulersFacade {
    fun main(): Scheduler
    fun io(): Scheduler
}