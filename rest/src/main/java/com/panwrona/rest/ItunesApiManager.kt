package com.panwrona.rest

import com.panwrona.domain.model.DomainItunesSong
import com.panwrona.domain.repository.ItunesRepository
import com.panwrona.rest.model.RestMapper
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val ITUNES_API_URL = "https://itunes.apple.com/"
object ItunesApiManager : ItunesRepository {

    private val okHttp = OkHttpClient.Builder()
            .addInterceptor(HeaderInterceptor())
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .connectTimeout(10, TimeUnit.SECONDS)
            .build()

    private val retrofit = Retrofit.Builder()
            .baseUrl(ITUNES_API_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttp)
            .build()

    private val restService = retrofit.create(ItunesService::class.java)

    override fun searchForSong(query: String): Observable<List<DomainItunesSong>> {
        return restService.searchForSong(query).map(RestMapper::toSearchDomain)
    }
}