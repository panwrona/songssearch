package com.panwrona.rest

import com.panwrona.rest.model.RestItunesSongsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ItunesService {

    @GET("search?&attribute=songTerm")
    fun searchForSong(
            @Query("term") query: String
    ): Observable<RestItunesSongsResponse>
}