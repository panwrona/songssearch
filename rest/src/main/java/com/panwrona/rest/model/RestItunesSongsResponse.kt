package com.panwrona.rest.model

import com.google.gson.annotations.SerializedName

data class RestItunesSongsResponse(
     @SerializedName("results") val results: List<RestItunesSong>
)