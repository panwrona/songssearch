package com.panwrona.rest.model

import com.panwrona.domain.model.DomainItunesSong

object RestMapper {

    fun toSearchDomain(response: RestItunesSongsResponse): List<DomainItunesSong> {
        val domainSongs = ArrayList<DomainItunesSong>()
        response.results.forEach {
            domainSongs.add(DomainItunesSong(
                    artist = it.artist ?: "Unknown artist",
                    description = it.description ?: "",
                    title = it.title ?: "Unknown title"
            ))
        }
        return domainSongs
    }
}