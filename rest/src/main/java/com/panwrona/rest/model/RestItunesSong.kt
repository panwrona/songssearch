package com.panwrona.rest.model

import com.google.gson.annotations.SerializedName

data class RestItunesSong(
        @SerializedName("artistName") val artist: String?,
        @SerializedName("trackName") val title: String?,
        @SerializedName("collectionName") val description: String?
)